package com.example.mywechat_list;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Email;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.CommonDataKinds.StructuredName;
import android.provider.ContactsContract.RawContacts.Data;
import android.support.annotation.RequiresApi;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;

public class MainActivity extends Activity implements View.OnClickListener {

    private Fragment mTab01 = new weixinFragment() ;
    private Fragment mTab02 = new frdFragment() ;
    private Fragment mTab03 = new contactFragment();
    private Fragment mTab04 = new settingsFragment() ;

    private FragmentManager fm;
    private LinearLayout mTabWeixin;
    private LinearLayout mTabFrd;
    private LinearLayout mTabContact;
    private LinearLayout mTabSettings;
    private ImageButton mImgWeixin;
    private ImageButton mImgFrd;
    private ImageButton mImgContact;
    private ImageButton mImgSettings;
    private ImageButton mImgAddContact;
    private EditText et_name;
    private EditText et_phone;
    private EditText et_email;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);

        int hasWriteContactsPermisson = checkSelfPermission(
                android.Manifest.permission.READ_CONTACTS);
        if (hasWriteContactsPermisson != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]
                            {Manifest.permission.WRITE_CONTACTS},
                    1);
            return;
        }

        initView();
        initEvent();
        initFragment();
        selectfragment(0);

    }

       private void initFragment(){
        fm = getFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.add(R.id.id_content,mTab01);
        transaction.add(R.id.id_content,mTab02);
        transaction.add(R.id.id_content,mTab03);
        transaction.add(R.id.id_content,mTab04);
        transaction.commit();

    }

    private void initView(){
        mTabWeixin = (LinearLayout) findViewById(R.id.id_tab_weixin);
        mTabFrd = (LinearLayout) findViewById(R.id.id_tab_frd);
        mTabContact =(LinearLayout) findViewById(R.id.id_tab_contact);
        mTabSettings =(LinearLayout) findViewById(R.id.id_tab_setting);

        mImgWeixin =(ImageButton)findViewById(R.id.id_tab_weixin_img);
        mImgFrd =(ImageButton)findViewById(R.id.id_tab_frd_img);
        mImgContact =(ImageButton)findViewById(R.id.id_tab_contact_img);
        mImgSettings = (ImageButton)findViewById(R.id.id_tab_settings_img);

        mImgAddContact=(ImageButton)findViewById(R.id.id_add_contact);
       // et_name = (EditText)findViewById(R.id.et_name);
      //  et_phone= (EditText)findViewById(R.id.et_phone);
      //  et_email = (EditText)findViewById(R.id.et_email);

    }

    //先隐藏所有页面，再根据传参判断显示的页面
    private void selectfragment(int i){
        FragmentTransaction transaction=fm.beginTransaction();
        hidefragment(transaction);
        switch(i){
            case 0:
                transaction.show(mTab01);
                mImgWeixin.setImageResource(R.drawable.tab_weixin_pressed);
                break;
            case 1:
                transaction.show(mTab02);
                mImgFrd.setImageResource(R.drawable.tab_find_frd_pressed);
                break;
            case 2:
                transaction.show(mTab03);
                mImgContact.setImageResource(R.drawable.tab_address_pressed);
                break;
            case 3:
                transaction.show(mTab04);
                mImgSettings.setImageResource(R.drawable.tab_settings_pressed);
                break;
            default:break;
        }
        transaction.commit();
    }

    //隐藏所有页面
    private void hidefragment(FragmentTransaction transaction){
        transaction.hide(mTab01);
        transaction.hide(mTab02);
        transaction.hide(mTab03);
        transaction.hide(mTab04);
    }


    //判断应该显示的页面，为显示函数传参
    @Override
    public void onClick(View v){
        resetimg();
        switch(v.getId()){
            case R.id.id_tab_weixin:
                selectfragment(0);
                break;
            case R.id.id_tab_frd:
                selectfragment(1);
                break;
            case R.id.id_tab_contact:
                selectfragment(2);
                break;
            case R.id.id_tab_setting:
                selectfragment(3);
                break;
            case R.id.id_add_contact:
                final View view1 = getLayoutInflater().inflate(R.layout.addcontact, null);
                new AlertDialog.Builder(MainActivity.this).setTitle("添加手机联系人信息").setView(view1).setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int which) {
                        et_name = (EditText)view1.findViewById(R.id.et_name);
                        et_phone = (EditText)view1.findViewById(R.id.et_phone);
                        et_email=(EditText)view1.findViewById(R.id.et_email);
                         String name = et_name.getText().toString();
                        final  String phone = et_phone.getText().toString();
                        final  String email = et_email.getText().toString();
                        // 创建一个空的ContentValues
                        ContentValues values = new ContentValues();
                        // 向RawContacts.CONTENT_URI执行一个空值插入
                        // 目的是获取系统返回的rawContactId
                        Uri rawContactUri = getContentResolver().insert(ContactsContract.RawContacts.CONTENT_URI, values);
                        long rawContactId = ContentUris.parseId(rawContactUri);
                        values.clear();

                        values.put(Data.RAW_CONTACT_ID, rawContactId);
                        // 设置内容类型
                        values.put(Data.MIMETYPE, StructuredName.CONTENT_ITEM_TYPE);
                        // 设置联系人名字
                        values.put(StructuredName.GIVEN_NAME, name);
                        // 向联系人URI添加联系人名字
                        getContentResolver().insert(ContactsContract
                                .Data.CONTENT_URI, values);
                        values.clear();

                        values.put(Data.RAW_CONTACT_ID, rawContactId);
                        values.put(Data.MIMETYPE, Phone.CONTENT_ITEM_TYPE);
                        // 设置联系人的电话号码
                        values.put(Phone.NUMBER, phone);
                        // 设置电话类型
                        values.put(Phone.TYPE, Phone.TYPE_MOBILE);
                        // 向联系人电话号码URI添加电话号码
                        getContentResolver().insert(ContactsContract.Data.CONTENT_URI, values);
                        values.clear();

                        values.put(Data.RAW_CONTACT_ID, rawContactId);
                        values.put(Data.MIMETYPE, Email.CONTENT_ITEM_TYPE);
                        // 设置联系人的E-mail地址
                        values.put(Email.DATA, email);
                        // 设置该电子邮件的类型
                        values.put(Email.TYPE, Email.TYPE_WORK);
                        // 向联系人E-mail URI添加E-mail数据
                        getContentResolver().insert(ContactsContract.Data.CONTENT_URI, values);
                    }
                }).show();
                break;
            case R.id.srl:

            default:
                break;
        }

    }

    //点击时将所有图标变灰
    public void resetimg(){
        mImgWeixin.setImageResource(R.drawable.tab_weixin_normal);
        mImgFrd.setImageResource(R.drawable.tab_find_frd_normal);
        mImgContact.setImageResource(R.drawable.tab_address_normal);
        mImgSettings.setImageResource(R.drawable.tab_settings_normal);
    }

    //监听过程
    private void initEvent(){
        mTabWeixin.setOnClickListener(this);
        mTabContact.setOnClickListener(this);
        mTabFrd.setOnClickListener(this);
        mTabSettings.setOnClickListener(this);
        mImgAddContact.setOnClickListener(this);
    }

}
