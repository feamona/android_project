package com.example.mywechat_list.adapter1;

import com.example.mywechat_list.DataBean;


public interface ItemClickListener {
    void onExpandChildren(DataBean bean);

    /**
     * 隐藏子Item
     * @param bean
     */
    void onHideChildren(DataBean bean);
}
