package com.example.mywechat_list;


import android.app.Fragment;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.annotation.RequiresApi;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.mywechat_list.adapter1.RecyclerAdapter;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class contactFragment extends Fragment {
    private List<String> mlist = new ArrayList<>();
    private RecyclerView mRecyclerView;
    private List<DataBean> dataBeanList;
    private DataBean dataBean;
    private RecyclerAdapter mAdapter;
    private SwipeRefreshLayout msrl;

    public contactFragment() {
        // Required empty public constructor
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       View view = inflater.inflate(R.layout.tab03, container, false);
        mRecyclerView = view.findViewById(R.id.rcv_expandcollapse);

        msrl = view.findViewById(R.id.srl);
        showlist();
        setData();
        msrl.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        showlist();
                       setData();
                       msrl.setRefreshing(false);
                    }
                },1200);

            }
        });

        return view;
    }

    public void showlist(){
         dataBeanList = new ArrayList<>();
        Cursor cursor = getActivity().getContentResolver().query(ContactsContract.Contacts.CONTENT_URI,
                null,null,null,null);
        while(cursor.moveToNext()){
            String contactId = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
            String name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
            dataBean = new DataBean();
            dataBean.setID(contactId);

            dataBean.setType(0);
            dataBean.setChildBean(dataBean);
            dataBean.setParentLeftTxt("Name:"+name);


            Cursor phones = getActivity().getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,
                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID+"="+contactId,null,null);
            while(phones.moveToNext()){
                String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                dataBean.setChildLeftTxt("MobilePhone:"+phoneNumber);
            }
            phones.close();

            Cursor emails = getActivity().getContentResolver().query(
                    ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                    null, ContactsContract.CommonDataKinds.Email
                            .CONTACT_ID + " = " + contactId, null, null);
            while (emails.moveToNext())
            {
                // 获取查询结果中E-mail地址列中数据
                String emailAddress = emails.getString(emails
                        .getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                dataBean.setChildRightTxt("Email:"+emailAddress);
            }
            emails.close();

            dataBeanList.add(dataBean);
        }
        cursor.close();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void setData(){
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mAdapter = new RecyclerAdapter(getContext(),dataBeanList);
        mRecyclerView.setAdapter(mAdapter);
        //滚动监听
        mAdapter.setOnScrollListener(new RecyclerAdapter.OnScrollListener() {
            @Override
            public void scrollTo(int pos) {
                mRecyclerView.scrollToPosition(pos);
            }
        });
    }


}
