package com.example.mywechat_list;


import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class weixinFragment extends Fragment {

    private List<String> mlist = new ArrayList<>();

    public weixinFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.tab01, container, false);

        adapter myadapter = new adapter(getActivity());
        RecyclerView rcvExpandCollapse = view.findViewById(R.id.rcv_expandcollapse);

        rcvExpandCollapse.setLayoutManager(new LinearLayoutManager(getActivity()));
        rcvExpandCollapse.setHasFixedSize(true);
        rcvExpandCollapse.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        rcvExpandCollapse.setAdapter(myadapter);

        myadapter.setExpandCollapseDataList(mlist);

        initview();
        return view;
    }

    private void initview(){
        mlist.add("美丽人生");
        mlist.add("活着");
        mlist.add("乱世佳人");
        mlist.add("闻香识女人");
        mlist.add("让子弹飞");
        mlist.add("那年那兔那些事");
    }

}
