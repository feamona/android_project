package com.example.mywechat;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.LinearLayout;


public class MainActivity extends Activity implements View.OnClickListener {

    private Fragment mTab01 = new weixinFragment() ;
    private Fragment mTab02 = new frdFragment() ;
    private Fragment mTab03 = new contactFragment();
    private Fragment mTab04 = new settingsFragment() ;

    private FragmentManager fm;
    private LinearLayout mTabWeixin;
    private LinearLayout mTabFrd;
    private LinearLayout mTabContact;
    private LinearLayout mTabSettings;
    private ImageButton mImgWeixin;
    private ImageButton mImgFrd;
    private ImageButton mImgContact;
    private ImageButton mImgSettings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);

        initView();
        initEvent();
        initFragment();
        selectfragment(0);
    }


    private void initFragment(){
        fm = getFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.add(R.id.id_content,mTab01);
        transaction.add(R.id.id_content,mTab02);
        transaction.add(R.id.id_content,mTab03);
        transaction.add(R.id.id_content,mTab04);
        transaction.commit();

    }

    private void initView(){
        mTabWeixin = (LinearLayout) findViewById(R.id.id_tab_weixin);
        mTabFrd = (LinearLayout) findViewById(R.id.id_tab_frd);
        mTabContact =(LinearLayout) findViewById(R.id.id_tab_contact);
        mTabSettings =(LinearLayout) findViewById(R.id.id_tab_setting);

        mImgWeixin =(ImageButton)findViewById(R.id.id_tab_weixin_img);
        mImgFrd =(ImageButton)findViewById(R.id.id_tab_frd_img);
        mImgContact =(ImageButton)findViewById(R.id.id_tab_contact_img);
        mImgSettings = (ImageButton)findViewById(R.id.id_tab_settings_img);

    }

    //先隐藏所有页面，再根据传参判断显示的页面
    private void selectfragment(int i){
        FragmentTransaction transaction=fm.beginTransaction();
        hidefragment(transaction);
        switch(i){
            case 0:
                transaction.show(mTab01);
                mImgWeixin.setImageResource(R.drawable.tab_weixin_pressed);
                break;
            case 1:
                transaction.show(mTab02);
                mImgFrd.setImageResource(R.drawable.tab_find_frd_pressed);
                break;
            case 2:
                transaction.show(mTab03);
                mImgContact.setImageResource(R.drawable.tab_address_pressed);
                break;
            case 3:
                transaction.show(mTab04);
                mImgSettings.setImageResource(R.drawable.tab_settings_pressed);
                break;
            default:break;
        }
        transaction.commit();
    }

    //隐藏所有页面
    private void hidefragment(FragmentTransaction transaction){
        transaction.hide(mTab01);
        transaction.hide(mTab02);
        transaction.hide(mTab03);
        transaction.hide(mTab04);
    }


    //判断应该显示的页面，为显示函数传参
    @Override
    public void onClick(View v){
        resetimg();
        switch(v.getId()){
            case R.id.id_tab_weixin:
                selectfragment(0);
                break;
            case R.id.id_tab_frd:
                selectfragment(1);
                break;
            case R.id.id_tab_contact:
                selectfragment(2);
                break;
            case R.id.id_tab_setting:
                selectfragment(3);
                break;
            default:
                break;
        }
    }

    //点击时将所有图标变灰
    public void resetimg(){
        mImgWeixin.setImageResource(R.drawable.tab_weixin_normal);
        mImgFrd.setImageResource(R.drawable.tab_find_frd_normal);
        mImgContact.setImageResource(R.drawable.tab_address_normal);
        mImgSettings.setImageResource(R.drawable.tab_settings_normal);
    }

    //监听过程
    private void initEvent(){
        mTabWeixin.setOnClickListener(this);
        mTabContact.setOnClickListener(this);
        mTabFrd.setOnClickListener(this);
        mTabSettings.setOnClickListener(this);
    }
}
